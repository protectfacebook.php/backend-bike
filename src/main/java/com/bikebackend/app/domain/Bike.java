package com.bikebackend.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import com.bikebackend.app.domain.enumeration.TypeShockAbsorber;

/**
 * A Bike.
 */
@Entity
@Table(name = "bike")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Bike implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "model", nullable = false)
    private String model;

    @Column(name = "price")
    private Double price;

    @Column(name = "serial")
    private String serial;

    @Column(name = "status")
    private Boolean status;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_shock_absorber")
    private TypeShockAbsorber typeShockAbsorber;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public Bike model(String model) {
        this.model = model;
        return this;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getPrice() {
        return price;
    }

    public Bike price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSerial() {
        return serial;
    }

    public Bike serial(String serial) {
        this.serial = serial;
        return this;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Boolean isStatus() {
        return status;
    }

    public Bike status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public TypeShockAbsorber getTypeShockAbsorber() {
        return typeShockAbsorber;
    }

    public Bike typeShockAbsorber(TypeShockAbsorber typeShockAbsorber) {
        this.typeShockAbsorber = typeShockAbsorber;
        return this;
    }

    public void setTypeShockAbsorber(TypeShockAbsorber typeShockAbsorber) {
        this.typeShockAbsorber = typeShockAbsorber;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Bike)) {
            return false;
        }
        return id != null && id.equals(((Bike) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Bike{" +
            "id=" + getId() +
            ", model='" + getModel() + "'" +
            ", price=" + getPrice() +
            ", serial='" + getSerial() + "'" +
            ", status='" + isStatus() + "'" +
            ", typeShockAbsorber='" + getTypeShockAbsorber() + "'" +
            "}";
    }
}
