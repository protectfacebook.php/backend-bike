package com.bikebackend.app.domain.enumeration;

/**
 * The TypeShockAbsorber enumeration.
 */
public enum TypeShockAbsorber {
    RIGIDA, HARDTAIL, FULL_SUSPENSION
}
