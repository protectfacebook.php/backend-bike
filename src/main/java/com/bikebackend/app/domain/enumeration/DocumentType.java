package com.bikebackend.app.domain.enumeration;

/**
 * The DocumentType enumeration.
 */
public enum DocumentType {
    CC, CE, TI, PASAPORTE
}
