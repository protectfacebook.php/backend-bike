package com.bikebackend.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A Sale.
 */
@Entity
@Table(name = "sale")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Sale implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "date_sale")
    private Instant dateSale;

    @ManyToOne
    @JsonIgnoreProperties(value = "sales", allowSetters = true)
    private Bike bike;

    @ManyToOne
    @JsonIgnoreProperties(value = "sales", allowSetters = true)
    private User client;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDateSale() {
        return dateSale;
    }

    public Sale dateSale(Instant dateSale) {
        this.dateSale = dateSale;
        return this;
    }

    public void setDateSale(Instant dateSale) {
        this.dateSale = dateSale;
    }

    public Bike getBike() {
        return bike;
    }

    public Sale bike(Bike bike) {
        this.bike = bike;
        return this;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public User getClient() {
        return client;
    }

    public Sale client(User user) {
        this.client = user;
        return this;
    }

    public void setClient(User user) {
        this.client = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Sale)) {
            return false;
        }
        return id != null && id.equals(((Sale) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Sale{" +
            "id=" + getId() +
            ", dateSale='" + getDateSale() + "'" +
            "}";
    }
}
