package com.bikebackend.app.repository;

import com.bikebackend.app.domain.Sale;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Sale entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SaleRepository extends JpaRepository<Sale, Long>, JpaSpecificationExecutor<Sale> {

    @Query("select sale from Sale sale where sale.client.login = ?#{principal.username}")
    List<Sale> findByClientIsCurrentUser();
}
