package com.bikebackend.app.repository;

import com.bikebackend.app.domain.DetailUser;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the DetailUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DetailUserRepository extends JpaRepository<DetailUser, Long>, JpaSpecificationExecutor<DetailUser> {
}
