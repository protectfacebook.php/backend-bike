package com.bikebackend.app.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.bikebackend.app.domain.Bike;
import com.bikebackend.app.domain.*; // for static metamodels
import com.bikebackend.app.repository.BikeRepository;
import com.bikebackend.app.service.dto.BikeCriteria;
import com.bikebackend.app.service.dto.BikeDTO;
import com.bikebackend.app.service.mapper.BikeMapper;

/**
 * Service for executing complex queries for {@link Bike} entities in the database.
 * The main input is a {@link BikeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BikeDTO} or a {@link Page} of {@link BikeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BikeQueryService extends QueryService<Bike> {

    private final Logger log = LoggerFactory.getLogger(BikeQueryService.class);

    private final BikeRepository bikeRepository;

    private final BikeMapper bikeMapper;

    public BikeQueryService(BikeRepository bikeRepository, BikeMapper bikeMapper) {
        this.bikeRepository = bikeRepository;
        this.bikeMapper = bikeMapper;
    }

    /**
     * Return a {@link List} of {@link BikeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BikeDTO> findByCriteria(BikeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Bike> specification = createSpecification(criteria);
        return bikeMapper.toDto(bikeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BikeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BikeDTO> findByCriteria(BikeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Bike> specification = createSpecification(criteria);
        return bikeRepository.findAll(specification, page)
            .map(bikeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BikeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Bike> specification = createSpecification(criteria);
        return bikeRepository.count(specification);
    }

    /**
     * Function to convert {@link BikeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Bike> createSpecification(BikeCriteria criteria) {
        Specification<Bike> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Bike_.id));
            }
            if (criteria.getModel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getModel(), Bike_.model));
            }
            if (criteria.getPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPrice(), Bike_.price));
            }
            if (criteria.getSerial() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSerial(), Bike_.serial));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), Bike_.status));
            }
            if (criteria.getTypeShockAbsorber() != null) {
                specification = specification.and(buildSpecification(criteria.getTypeShockAbsorber(), Bike_.typeShockAbsorber));
            }
        }
        return specification;
    }
}
