package com.bikebackend.app.service.mapper;


import com.bikebackend.app.domain.*;
import com.bikebackend.app.service.dto.SaleDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Sale} and its DTO {@link SaleDTO}.
 */
@Mapper(componentModel = "spring", uses = {BikeMapper.class, UserMapper.class})
public interface SaleMapper extends EntityMapper<SaleDTO, Sale> {

    @Mapping(source = "bike.id", target = "bikeId")
    @Mapping(source = "bike.model", target = "bikeModel")
    @Mapping(source = "client.id", target = "clientId")
    @Mapping(source = "client.firstName", target = "clientFirstName")
    SaleDTO toDto(Sale sale);

    @Mapping(source = "bikeId", target = "bike")
    @Mapping(source = "clientId", target = "client")
    Sale toEntity(SaleDTO saleDTO);

    default Sale fromId(Long id) {
        if (id == null) {
            return null;
        }
        Sale sale = new Sale();
        sale.setId(id);
        return sale;
    }
}
