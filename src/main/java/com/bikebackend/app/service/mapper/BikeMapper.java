package com.bikebackend.app.service.mapper;


import com.bikebackend.app.domain.*;
import com.bikebackend.app.service.dto.BikeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Bike} and its DTO {@link BikeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BikeMapper extends EntityMapper<BikeDTO, Bike> {



    default Bike fromId(Long id) {
        if (id == null) {
            return null;
        }
        Bike bike = new Bike();
        bike.setId(id);
        return bike;
    }
}
