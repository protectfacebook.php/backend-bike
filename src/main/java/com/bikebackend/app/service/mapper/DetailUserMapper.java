package com.bikebackend.app.service.mapper;


import com.bikebackend.app.domain.*;
import com.bikebackend.app.service.dto.DetailUserDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DetailUser} and its DTO {@link DetailUserDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface DetailUserMapper extends EntityMapper<DetailUserDTO, DetailUser> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.firstName", target = "userFirstName")
    DetailUserDTO toDto(DetailUser detailUser);

    @Mapping(source = "userId", target = "user")
    DetailUser toEntity(DetailUserDTO detailUserDTO);

    default DetailUser fromId(Long id) {
        if (id == null) {
            return null;
        }
        DetailUser detailUser = new DetailUser();
        detailUser.setId(id);
        return detailUser;
    }
}
