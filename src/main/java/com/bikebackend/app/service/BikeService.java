package com.bikebackend.app.service;

import com.bikebackend.app.service.dto.BikeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.bikebackend.app.domain.Bike}.
 */
public interface BikeService {

    /**
     * Save a bike.
     *
     * @param bikeDTO the entity to save.
     * @return the persisted entity.
     */
    BikeDTO save(BikeDTO bikeDTO);

    /**
     * Get all the bikes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BikeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" bike.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BikeDTO> findOne(Long id);

    /**
     * Delete the "id" bike.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
