package com.bikebackend.app.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.bikebackend.app.domain.DetailUser;
import com.bikebackend.app.domain.*; // for static metamodels
import com.bikebackend.app.repository.DetailUserRepository;
import com.bikebackend.app.service.dto.DetailUserCriteria;
import com.bikebackend.app.service.dto.DetailUserDTO;
import com.bikebackend.app.service.mapper.DetailUserMapper;

/**
 * Service for executing complex queries for {@link DetailUser} entities in the database.
 * The main input is a {@link DetailUserCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DetailUserDTO} or a {@link Page} of {@link DetailUserDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DetailUserQueryService extends QueryService<DetailUser> {

    private final Logger log = LoggerFactory.getLogger(DetailUserQueryService.class);

    private final DetailUserRepository detailUserRepository;

    private final DetailUserMapper detailUserMapper;

    public DetailUserQueryService(DetailUserRepository detailUserRepository, DetailUserMapper detailUserMapper) {
        this.detailUserRepository = detailUserRepository;
        this.detailUserMapper = detailUserMapper;
    }

    /**
     * Return a {@link List} of {@link DetailUserDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DetailUserDTO> findByCriteria(DetailUserCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DetailUser> specification = createSpecification(criteria);
        return detailUserMapper.toDto(detailUserRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DetailUserDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DetailUserDTO> findByCriteria(DetailUserCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DetailUser> specification = createSpecification(criteria);
        return detailUserRepository.findAll(specification, page)
            .map(detailUserMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DetailUserCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DetailUser> specification = createSpecification(criteria);
        return detailUserRepository.count(specification);
    }

    /**
     * Function to convert {@link DetailUserCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DetailUser> createSpecification(DetailUserCriteria criteria) {
        Specification<DetailUser> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DetailUser_.id));
            }
            if (criteria.getDocumentType() != null) {
                specification = specification.and(buildSpecification(criteria.getDocumentType(), DetailUser_.documentType));
            }
            if (criteria.getDocumentNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDocumentNumber(), DetailUser_.documentNumber));
            }
            if (criteria.getExpeditionDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getExpeditionDate(), DetailUser_.expeditionDate));
            }
            if (criteria.getCellphone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCellphone(), DetailUser_.cellphone));
            }
            if (criteria.getGender() != null) {
                specification = specification.and(buildSpecification(criteria.getGender(), DetailUser_.gender));
            }
            if (criteria.getBirthdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBirthdate(), DetailUser_.birthdate));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(DetailUser_.user, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
