package com.bikebackend.app.service.impl;

import com.bikebackend.app.service.BikeService;
import com.bikebackend.app.domain.Bike;
import com.bikebackend.app.repository.BikeRepository;
import com.bikebackend.app.service.dto.BikeDTO;
import com.bikebackend.app.service.mapper.BikeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Bike}.
 */
@Service
@Transactional
public class BikeServiceImpl implements BikeService {

    private final Logger log = LoggerFactory.getLogger(BikeServiceImpl.class);

    private final BikeRepository bikeRepository;

    private final BikeMapper bikeMapper;

    public BikeServiceImpl(BikeRepository bikeRepository, BikeMapper bikeMapper) {
        this.bikeRepository = bikeRepository;
        this.bikeMapper = bikeMapper;
    }

    @Override
    public BikeDTO save(BikeDTO bikeDTO) {
        log.debug("Request to save Bike : {}", bikeDTO);
        Bike bike = bikeMapper.toEntity(bikeDTO);
        bike = bikeRepository.save(bike);
        return bikeMapper.toDto(bike);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BikeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Bikes");
        return bikeRepository.findAll(pageable)
            .map(bikeMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<BikeDTO> findOne(Long id) {
        log.debug("Request to get Bike : {}", id);
        return bikeRepository.findById(id)
            .map(bikeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Bike : {}", id);
        bikeRepository.deleteById(id);
    }
}
