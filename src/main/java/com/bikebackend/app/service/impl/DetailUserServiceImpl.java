package com.bikebackend.app.service.impl;

import com.bikebackend.app.service.DetailUserService;
import com.bikebackend.app.domain.DetailUser;
import com.bikebackend.app.repository.DetailUserRepository;
import com.bikebackend.app.service.dto.DetailUserDTO;
import com.bikebackend.app.service.mapper.DetailUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link DetailUser}.
 */
@Service
@Transactional
public class DetailUserServiceImpl implements DetailUserService {

    private final Logger log = LoggerFactory.getLogger(DetailUserServiceImpl.class);

    private final DetailUserRepository detailUserRepository;

    private final DetailUserMapper detailUserMapper;

    public DetailUserServiceImpl(DetailUserRepository detailUserRepository, DetailUserMapper detailUserMapper) {
        this.detailUserRepository = detailUserRepository;
        this.detailUserMapper = detailUserMapper;
    }

    @Override
    public DetailUserDTO save(DetailUserDTO detailUserDTO) {
        log.debug("Request to save DetailUser : {}", detailUserDTO);
        DetailUser detailUser = detailUserMapper.toEntity(detailUserDTO);
        detailUser = detailUserRepository.save(detailUser);
        return detailUserMapper.toDto(detailUser);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DetailUserDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DetailUsers");
        return detailUserRepository.findAll(pageable)
            .map(detailUserMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<DetailUserDTO> findOne(Long id) {
        log.debug("Request to get DetailUser : {}", id);
        return detailUserRepository.findById(id)
            .map(detailUserMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DetailUser : {}", id);
        detailUserRepository.deleteById(id);
    }
}
