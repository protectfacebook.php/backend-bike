package com.bikebackend.app.service.dto;

import java.time.Instant;
import java.io.Serializable;

/**
 * A DTO for the {@link com.bikebackend.app.domain.Sale} entity.
 */
public class SaleDTO implements Serializable {
    
    private Long id;

    private Instant dateSale;


    private Long bikeId;

    private String bikeModel;

    private Long clientId;

    private String clientFirstName;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDateSale() {
        return dateSale;
    }

    public void setDateSale(Instant dateSale) {
        this.dateSale = dateSale;
    }

    public Long getBikeId() {
        return bikeId;
    }

    public void setBikeId(Long bikeId) {
        this.bikeId = bikeId;
    }

    public String getBikeModel() {
        return bikeModel;
    }

    public void setBikeModel(String bikeModel) {
        this.bikeModel = bikeModel;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long userId) {
        this.clientId = userId;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public void setClientFirstName(String userFirstName) {
        this.clientFirstName = userFirstName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SaleDTO)) {
            return false;
        }

        return id != null && id.equals(((SaleDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SaleDTO{" +
            "id=" + getId() +
            ", dateSale='" + getDateSale() + "'" +
            ", bikeId=" + getBikeId() +
            ", bikeModel='" + getBikeModel() + "'" +
            ", clientId=" + getClientId() +
            ", clientFirstName='" + getClientFirstName() + "'" +
            "}";
    }
}
