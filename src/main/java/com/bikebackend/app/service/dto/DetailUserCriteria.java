package com.bikebackend.app.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.bikebackend.app.domain.enumeration.DocumentType;
import com.bikebackend.app.domain.enumeration.Gender;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.bikebackend.app.domain.DetailUser} entity. This class is used
 * in {@link com.bikebackend.app.web.rest.DetailUserResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /detail-users?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DetailUserCriteria implements Serializable, Criteria {
    /**
     * Class for filtering DocumentType
     */
    public static class DocumentTypeFilter extends Filter<DocumentType> {

        public DocumentTypeFilter() {
        }

        public DocumentTypeFilter(DocumentTypeFilter filter) {
            super(filter);
        }

        @Override
        public DocumentTypeFilter copy() {
            return new DocumentTypeFilter(this);
        }

    }
    /**
     * Class for filtering Gender
     */
    public static class GenderFilter extends Filter<Gender> {

        public GenderFilter() {
        }

        public GenderFilter(GenderFilter filter) {
            super(filter);
        }

        @Override
        public GenderFilter copy() {
            return new GenderFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DocumentTypeFilter documentType;

    private StringFilter documentNumber;

    private LocalDateFilter expeditionDate;

    private StringFilter cellphone;

    private GenderFilter gender;

    private LocalDateFilter birthdate;

    private LongFilter userId;

    public DetailUserCriteria() {
    }

    public DetailUserCriteria(DetailUserCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.documentType = other.documentType == null ? null : other.documentType.copy();
        this.documentNumber = other.documentNumber == null ? null : other.documentNumber.copy();
        this.expeditionDate = other.expeditionDate == null ? null : other.expeditionDate.copy();
        this.cellphone = other.cellphone == null ? null : other.cellphone.copy();
        this.gender = other.gender == null ? null : other.gender.copy();
        this.birthdate = other.birthdate == null ? null : other.birthdate.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public DetailUserCriteria copy() {
        return new DetailUserCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DocumentTypeFilter getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentTypeFilter documentType) {
        this.documentType = documentType;
    }

    public StringFilter getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(StringFilter documentNumber) {
        this.documentNumber = documentNumber;
    }

    public LocalDateFilter getExpeditionDate() {
        return expeditionDate;
    }

    public void setExpeditionDate(LocalDateFilter expeditionDate) {
        this.expeditionDate = expeditionDate;
    }

    public StringFilter getCellphone() {
        return cellphone;
    }

    public void setCellphone(StringFilter cellphone) {
        this.cellphone = cellphone;
    }

    public GenderFilter getGender() {
        return gender;
    }

    public void setGender(GenderFilter gender) {
        this.gender = gender;
    }

    public LocalDateFilter getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDateFilter birthdate) {
        this.birthdate = birthdate;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DetailUserCriteria that = (DetailUserCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(documentType, that.documentType) &&
            Objects.equals(documentNumber, that.documentNumber) &&
            Objects.equals(expeditionDate, that.expeditionDate) &&
            Objects.equals(cellphone, that.cellphone) &&
            Objects.equals(gender, that.gender) &&
            Objects.equals(birthdate, that.birthdate) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        documentType,
        documentNumber,
        expeditionDate,
        cellphone,
        gender,
        birthdate,
        userId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DetailUserCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (documentType != null ? "documentType=" + documentType + ", " : "") +
                (documentNumber != null ? "documentNumber=" + documentNumber + ", " : "") +
                (expeditionDate != null ? "expeditionDate=" + expeditionDate + ", " : "") +
                (cellphone != null ? "cellphone=" + cellphone + ", " : "") +
                (gender != null ? "gender=" + gender + ", " : "") +
                (birthdate != null ? "birthdate=" + birthdate + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
