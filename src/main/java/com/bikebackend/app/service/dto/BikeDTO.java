package com.bikebackend.app.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import com.bikebackend.app.domain.enumeration.TypeShockAbsorber;

/**
 * A DTO for the {@link com.bikebackend.app.domain.Bike} entity.
 */
public class BikeDTO implements Serializable {
    
    private Long id;

    @NotNull
    private String model;

    private Double price;

    private String serial;

    private Boolean status;

    private TypeShockAbsorber typeShockAbsorber;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public TypeShockAbsorber getTypeShockAbsorber() {
        return typeShockAbsorber;
    }

    public void setTypeShockAbsorber(TypeShockAbsorber typeShockAbsorber) {
        this.typeShockAbsorber = typeShockAbsorber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BikeDTO)) {
            return false;
        }

        return id != null && id.equals(((BikeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BikeDTO{" +
            "id=" + getId() +
            ", model='" + getModel() + "'" +
            ", price=" + getPrice() +
            ", serial='" + getSerial() + "'" +
            ", status='" + isStatus() + "'" +
            ", typeShockAbsorber='" + getTypeShockAbsorber() + "'" +
            "}";
    }
}
