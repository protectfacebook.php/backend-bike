package com.bikebackend.app.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.bikebackend.app.domain.Sale} entity. This class is used
 * in {@link com.bikebackend.app.web.rest.SaleResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /sales?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SaleCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter dateSale;

    private LongFilter bikeId;

    private LongFilter clientId;

    public SaleCriteria() {
    }

    public SaleCriteria(SaleCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dateSale = other.dateSale == null ? null : other.dateSale.copy();
        this.bikeId = other.bikeId == null ? null : other.bikeId.copy();
        this.clientId = other.clientId == null ? null : other.clientId.copy();
    }

    @Override
    public SaleCriteria copy() {
        return new SaleCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getDateSale() {
        return dateSale;
    }

    public void setDateSale(InstantFilter dateSale) {
        this.dateSale = dateSale;
    }

    public LongFilter getBikeId() {
        return bikeId;
    }

    public void setBikeId(LongFilter bikeId) {
        this.bikeId = bikeId;
    }

    public LongFilter getClientId() {
        return clientId;
    }

    public void setClientId(LongFilter clientId) {
        this.clientId = clientId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SaleCriteria that = (SaleCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(dateSale, that.dateSale) &&
            Objects.equals(bikeId, that.bikeId) &&
            Objects.equals(clientId, that.clientId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        dateSale,
        bikeId,
        clientId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SaleCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (dateSale != null ? "dateSale=" + dateSale + ", " : "") +
                (bikeId != null ? "bikeId=" + bikeId + ", " : "") +
                (clientId != null ? "clientId=" + clientId + ", " : "") +
            "}";
    }

}
