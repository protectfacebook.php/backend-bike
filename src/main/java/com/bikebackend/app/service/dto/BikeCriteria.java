package com.bikebackend.app.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.bikebackend.app.domain.enumeration.TypeShockAbsorber;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.bikebackend.app.domain.Bike} entity. This class is used
 * in {@link com.bikebackend.app.web.rest.BikeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /bikes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BikeCriteria implements Serializable, Criteria {
    /**
     * Class for filtering TypeShockAbsorber
     */
    public static class TypeShockAbsorberFilter extends Filter<TypeShockAbsorber> {

        public TypeShockAbsorberFilter() {
        }

        public TypeShockAbsorberFilter(TypeShockAbsorberFilter filter) {
            super(filter);
        }

        @Override
        public TypeShockAbsorberFilter copy() {
            return new TypeShockAbsorberFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter model;

    private DoubleFilter price;

    private StringFilter serial;

    private BooleanFilter status;

    private TypeShockAbsorberFilter typeShockAbsorber;

    public BikeCriteria() {
    }

    public BikeCriteria(BikeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.model = other.model == null ? null : other.model.copy();
        this.price = other.price == null ? null : other.price.copy();
        this.serial = other.serial == null ? null : other.serial.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.typeShockAbsorber = other.typeShockAbsorber == null ? null : other.typeShockAbsorber.copy();
    }

    @Override
    public BikeCriteria copy() {
        return new BikeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getModel() {
        return model;
    }

    public void setModel(StringFilter model) {
        this.model = model;
    }

    public DoubleFilter getPrice() {
        return price;
    }

    public void setPrice(DoubleFilter price) {
        this.price = price;
    }

    public StringFilter getSerial() {
        return serial;
    }

    public void setSerial(StringFilter serial) {
        this.serial = serial;
    }

    public BooleanFilter getStatus() {
        return status;
    }

    public void setStatus(BooleanFilter status) {
        this.status = status;
    }

    public TypeShockAbsorberFilter getTypeShockAbsorber() {
        return typeShockAbsorber;
    }

    public void setTypeShockAbsorber(TypeShockAbsorberFilter typeShockAbsorber) {
        this.typeShockAbsorber = typeShockAbsorber;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BikeCriteria that = (BikeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(model, that.model) &&
            Objects.equals(price, that.price) &&
            Objects.equals(serial, that.serial) &&
            Objects.equals(status, that.status) &&
            Objects.equals(typeShockAbsorber, that.typeShockAbsorber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        model,
        price,
        serial,
        status,
        typeShockAbsorber
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BikeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (model != null ? "model=" + model + ", " : "") +
                (price != null ? "price=" + price + ", " : "") +
                (serial != null ? "serial=" + serial + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (typeShockAbsorber != null ? "typeShockAbsorber=" + typeShockAbsorber + ", " : "") +
            "}";
    }

}
