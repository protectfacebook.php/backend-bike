package com.bikebackend.app.service;

import com.bikebackend.app.service.dto.DetailUserDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.bikebackend.app.domain.DetailUser}.
 */
public interface DetailUserService {

    /**
     * Save a detailUser.
     *
     * @param detailUserDTO the entity to save.
     * @return the persisted entity.
     */
    DetailUserDTO save(DetailUserDTO detailUserDTO);

    /**
     * Get all the detailUsers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DetailUserDTO> findAll(Pageable pageable);


    /**
     * Get the "id" detailUser.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DetailUserDTO> findOne(Long id);

    /**
     * Delete the "id" detailUser.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
