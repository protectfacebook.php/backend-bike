/**
 * Data Access Objects used by WebSocket services.
 */
package com.bikebackend.app.web.websocket.dto;
