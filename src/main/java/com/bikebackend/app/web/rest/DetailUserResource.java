package com.bikebackend.app.web.rest;

import com.bikebackend.app.service.DetailUserService;
import com.bikebackend.app.web.rest.errors.BadRequestAlertException;
import com.bikebackend.app.service.dto.DetailUserDTO;
import com.bikebackend.app.service.dto.DetailUserCriteria;
import com.bikebackend.app.service.DetailUserQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.bikebackend.app.domain.DetailUser}.
 */
@RestController
@RequestMapping("/api")
public class DetailUserResource {

    private final Logger log = LoggerFactory.getLogger(DetailUserResource.class);

    private static final String ENTITY_NAME = "detailUser";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DetailUserService detailUserService;

    private final DetailUserQueryService detailUserQueryService;

    public DetailUserResource(DetailUserService detailUserService, DetailUserQueryService detailUserQueryService) {
        this.detailUserService = detailUserService;
        this.detailUserQueryService = detailUserQueryService;
    }

    /**
     * {@code POST  /detail-users} : Create a new detailUser.
     *
     * @param detailUserDTO the detailUserDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new detailUserDTO, or with status {@code 400 (Bad Request)} if the detailUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/detail-users")
    public ResponseEntity<DetailUserDTO> createDetailUser(@Valid @RequestBody DetailUserDTO detailUserDTO) throws URISyntaxException {
        log.debug("REST request to save DetailUser : {}", detailUserDTO);
        if (detailUserDTO.getId() != null) {
            throw new BadRequestAlertException("A new detailUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DetailUserDTO result = detailUserService.save(detailUserDTO);
        return ResponseEntity.created(new URI("/api/detail-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /detail-users} : Updates an existing detailUser.
     *
     * @param detailUserDTO the detailUserDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated detailUserDTO,
     * or with status {@code 400 (Bad Request)} if the detailUserDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the detailUserDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/detail-users")
    public ResponseEntity<DetailUserDTO> updateDetailUser(@Valid @RequestBody DetailUserDTO detailUserDTO) throws URISyntaxException {
        log.debug("REST request to update DetailUser : {}", detailUserDTO);
        if (detailUserDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DetailUserDTO result = detailUserService.save(detailUserDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, detailUserDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /detail-users} : get all the detailUsers.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of detailUsers in body.
     */
    @GetMapping("/detail-users")
    public ResponseEntity<List<DetailUserDTO>> getAllDetailUsers(DetailUserCriteria criteria, Pageable pageable) {
        log.debug("REST request to get DetailUsers by criteria: {}", criteria);
        Page<DetailUserDTO> page = detailUserQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /detail-users/count} : count all the detailUsers.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/detail-users/count")
    public ResponseEntity<Long> countDetailUsers(DetailUserCriteria criteria) {
        log.debug("REST request to count DetailUsers by criteria: {}", criteria);
        return ResponseEntity.ok().body(detailUserQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /detail-users/:id} : get the "id" detailUser.
     *
     * @param id the id of the detailUserDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the detailUserDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/detail-users/{id}")
    public ResponseEntity<DetailUserDTO> getDetailUser(@PathVariable Long id) {
        log.debug("REST request to get DetailUser : {}", id);
        Optional<DetailUserDTO> detailUserDTO = detailUserService.findOne(id);
        return ResponseUtil.wrapOrNotFound(detailUserDTO);
    }

    /**
     * {@code DELETE  /detail-users/:id} : delete the "id" detailUser.
     *
     * @param id the id of the detailUserDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/detail-users/{id}")
    public ResponseEntity<Void> deleteDetailUser(@PathVariable Long id) {
        log.debug("REST request to delete DetailUser : {}", id);
        detailUserService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
