/**
 * View Models used by Spring MVC REST controllers.
 */
package com.bikebackend.app.web.rest.vm;
