package com.bikebackend.app.web.rest;

import com.bikebackend.app.service.BikeService;
import com.bikebackend.app.web.rest.errors.BadRequestAlertException;
import com.bikebackend.app.service.dto.BikeDTO;
import com.bikebackend.app.service.dto.BikeCriteria;
import com.bikebackend.app.service.BikeQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.bikebackend.app.domain.Bike}.
 */
@RestController
@RequestMapping("/api")
public class BikeResource {

    private final Logger log = LoggerFactory.getLogger(BikeResource.class);

    private static final String ENTITY_NAME = "bike";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BikeService bikeService;

    private final BikeQueryService bikeQueryService;

    public BikeResource(BikeService bikeService, BikeQueryService bikeQueryService) {
        this.bikeService = bikeService;
        this.bikeQueryService = bikeQueryService;
    }

    /**
     * {@code POST  /bikes} : Create a new bike.
     *
     * @param bikeDTO the bikeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bikeDTO, or with status {@code 400 (Bad Request)} if the bike has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bikes")
    public ResponseEntity<BikeDTO> createBike(@Valid @RequestBody BikeDTO bikeDTO) throws URISyntaxException {
        log.debug("REST request to save Bike : {}", bikeDTO);
        if (bikeDTO.getId() != null) {
            throw new BadRequestAlertException("A new bike cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BikeDTO result = bikeService.save(bikeDTO);
        return ResponseEntity.created(new URI("/api/bikes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /bikes} : Updates an existing bike.
     *
     * @param bikeDTO the bikeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bikeDTO,
     * or with status {@code 400 (Bad Request)} if the bikeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bikeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bikes")
    public ResponseEntity<BikeDTO> updateBike(@Valid @RequestBody BikeDTO bikeDTO) throws URISyntaxException {
        log.debug("REST request to update Bike : {}", bikeDTO);
        if (bikeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BikeDTO result = bikeService.save(bikeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, bikeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /bikes} : get all the bikes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bikes in body.
     */
    @GetMapping("/bikes")
    public ResponseEntity<List<BikeDTO>> getAllBikes(BikeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Bikes by criteria: {}", criteria);
        Page<BikeDTO> page = bikeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /bikes/count} : count all the bikes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/bikes/count")
    public ResponseEntity<Long> countBikes(BikeCriteria criteria) {
        log.debug("REST request to count Bikes by criteria: {}", criteria);
        return ResponseEntity.ok().body(bikeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /bikes/:id} : get the "id" bike.
     *
     * @param id the id of the bikeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bikeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bikes/{id}")
    public ResponseEntity<BikeDTO> getBike(@PathVariable Long id) {
        log.debug("REST request to get Bike : {}", id);
        Optional<BikeDTO> bikeDTO = bikeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bikeDTO);
    }

    /**
     * {@code DELETE  /bikes/:id} : delete the "id" bike.
     *
     * @param id the id of the bikeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bikes/{id}")
    public ResponseEntity<Void> deleteBike(@PathVariable Long id) {
        log.debug("REST request to delete Bike : {}", id);
        bikeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
