import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ISale, Sale } from 'app/shared/model/sale.model';
import { SaleService } from './sale.service';
import { IBike } from 'app/shared/model/bike.model';
import { BikeService } from 'app/entities/bike/bike.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

type SelectableEntity = IBike | IUser;

@Component({
  selector: 'jhi-sale-update',
  templateUrl: './sale-update.component.html',
})
export class SaleUpdateComponent implements OnInit {
  isSaving = false;
  bikes: IBike[] = [];
  users: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    dateSale: [],
    bikeId: [],
    clientId: [],
  });

  constructor(
    protected saleService: SaleService,
    protected bikeService: BikeService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sale }) => {
      if (!sale.id) {
        const today = moment().startOf('day');
        sale.dateSale = today;
      }

      this.updateForm(sale);

      this.bikeService.query().subscribe((res: HttpResponse<IBike[]>) => (this.bikes = res.body || []));

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(sale: ISale): void {
    this.editForm.patchValue({
      id: sale.id,
      dateSale: sale.dateSale ? sale.dateSale.format(DATE_TIME_FORMAT) : null,
      bikeId: sale.bikeId,
      clientId: sale.clientId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const sale = this.createFromForm();
    if (sale.id !== undefined) {
      this.subscribeToSaveResponse(this.saleService.update(sale));
    } else {
      this.subscribeToSaveResponse(this.saleService.create(sale));
    }
  }

  private createFromForm(): ISale {
    return {
      ...new Sale(),
      id: this.editForm.get(['id'])!.value,
      dateSale: this.editForm.get(['dateSale'])!.value ? moment(this.editForm.get(['dateSale'])!.value, DATE_TIME_FORMAT) : undefined,
      bikeId: this.editForm.get(['bikeId'])!.value,
      clientId: this.editForm.get(['clientId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISale>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
