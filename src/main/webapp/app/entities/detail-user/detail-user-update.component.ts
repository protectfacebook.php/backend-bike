import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IDetailUser, DetailUser } from 'app/shared/model/detail-user.model';
import { DetailUserService } from './detail-user.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-detail-user-update',
  templateUrl: './detail-user-update.component.html',
})
export class DetailUserUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  expeditionDateDp: any;
  birthdateDp: any;

  editForm = this.fb.group({
    id: [],
    documentType: [],
    documentNumber: [null, [Validators.maxLength(15)]],
    expeditionDate: [],
    cellphone: [null, [Validators.maxLength(12)]],
    gender: [],
    image: [],
    imageContentType: [],
    birthdate: [],
    userId: [],
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected detailUserService: DetailUserService,
    protected userService: UserService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ detailUser }) => {
      this.updateForm(detailUser);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(detailUser: IDetailUser): void {
    this.editForm.patchValue({
      id: detailUser.id,
      documentType: detailUser.documentType,
      documentNumber: detailUser.documentNumber,
      expeditionDate: detailUser.expeditionDate,
      cellphone: detailUser.cellphone,
      gender: detailUser.gender,
      image: detailUser.image,
      imageContentType: detailUser.imageContentType,
      birthdate: detailUser.birthdate,
      userId: detailUser.userId,
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('bikeBackendApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const detailUser = this.createFromForm();
    if (detailUser.id !== undefined) {
      this.subscribeToSaveResponse(this.detailUserService.update(detailUser));
    } else {
      this.subscribeToSaveResponse(this.detailUserService.create(detailUser));
    }
  }

  private createFromForm(): IDetailUser {
    return {
      ...new DetailUser(),
      id: this.editForm.get(['id'])!.value,
      documentType: this.editForm.get(['documentType'])!.value,
      documentNumber: this.editForm.get(['documentNumber'])!.value,
      expeditionDate: this.editForm.get(['expeditionDate'])!.value,
      cellphone: this.editForm.get(['cellphone'])!.value,
      gender: this.editForm.get(['gender'])!.value,
      imageContentType: this.editForm.get(['imageContentType'])!.value,
      image: this.editForm.get(['image'])!.value,
      birthdate: this.editForm.get(['birthdate'])!.value,
      userId: this.editForm.get(['userId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDetailUser>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
