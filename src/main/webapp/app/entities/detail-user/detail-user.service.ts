import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDetailUser } from 'app/shared/model/detail-user.model';

type EntityResponseType = HttpResponse<IDetailUser>;
type EntityArrayResponseType = HttpResponse<IDetailUser[]>;

@Injectable({ providedIn: 'root' })
export class DetailUserService {
  public resourceUrl = SERVER_API_URL + 'api/detail-users';

  constructor(protected http: HttpClient) {}

  create(detailUser: IDetailUser): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(detailUser);
    return this.http
      .post<IDetailUser>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(detailUser: IDetailUser): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(detailUser);
    return this.http
      .put<IDetailUser>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDetailUser>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDetailUser[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(detailUser: IDetailUser): IDetailUser {
    const copy: IDetailUser = Object.assign({}, detailUser, {
      expeditionDate:
        detailUser.expeditionDate && detailUser.expeditionDate.isValid() ? detailUser.expeditionDate.format(DATE_FORMAT) : undefined,
      birthdate: detailUser.birthdate && detailUser.birthdate.isValid() ? detailUser.birthdate.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.expeditionDate = res.body.expeditionDate ? moment(res.body.expeditionDate) : undefined;
      res.body.birthdate = res.body.birthdate ? moment(res.body.birthdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((detailUser: IDetailUser) => {
        detailUser.expeditionDate = detailUser.expeditionDate ? moment(detailUser.expeditionDate) : undefined;
        detailUser.birthdate = detailUser.birthdate ? moment(detailUser.birthdate) : undefined;
      });
    }
    return res;
  }
}
