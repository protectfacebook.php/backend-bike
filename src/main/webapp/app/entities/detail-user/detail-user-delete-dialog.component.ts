import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDetailUser } from 'app/shared/model/detail-user.model';
import { DetailUserService } from './detail-user.service';

@Component({
  templateUrl: './detail-user-delete-dialog.component.html',
})
export class DetailUserDeleteDialogComponent {
  detailUser?: IDetailUser;

  constructor(
    protected detailUserService: DetailUserService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.detailUserService.delete(id).subscribe(() => {
      this.eventManager.broadcast('detailUserListModification');
      this.activeModal.close();
    });
  }
}
