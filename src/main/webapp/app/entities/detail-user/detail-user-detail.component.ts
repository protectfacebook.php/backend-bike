import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IDetailUser } from 'app/shared/model/detail-user.model';

@Component({
  selector: 'jhi-detail-user-detail',
  templateUrl: './detail-user-detail.component.html',
})
export class DetailUserDetailComponent implements OnInit {
  detailUser: IDetailUser | null = null;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ detailUser }) => (this.detailUser = detailUser));
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType = '', base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  previousState(): void {
    window.history.back();
  }
}
