import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BikeBackendSharedModule } from 'app/shared/shared.module';
import { DetailUserComponent } from './detail-user.component';
import { DetailUserDetailComponent } from './detail-user-detail.component';
import { DetailUserUpdateComponent } from './detail-user-update.component';
import { DetailUserDeleteDialogComponent } from './detail-user-delete-dialog.component';
import { detailUserRoute } from './detail-user.route';

@NgModule({
  imports: [BikeBackendSharedModule, RouterModule.forChild(detailUserRoute)],
  declarations: [DetailUserComponent, DetailUserDetailComponent, DetailUserUpdateComponent, DetailUserDeleteDialogComponent],
  entryComponents: [DetailUserDeleteDialogComponent],
})
export class BikeBackendDetailUserModule {}
