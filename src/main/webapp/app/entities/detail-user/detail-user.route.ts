import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IDetailUser, DetailUser } from 'app/shared/model/detail-user.model';
import { DetailUserService } from './detail-user.service';
import { DetailUserComponent } from './detail-user.component';
import { DetailUserDetailComponent } from './detail-user-detail.component';
import { DetailUserUpdateComponent } from './detail-user-update.component';

@Injectable({ providedIn: 'root' })
export class DetailUserResolve implements Resolve<IDetailUser> {
  constructor(private service: DetailUserService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDetailUser> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((detailUser: HttpResponse<DetailUser>) => {
          if (detailUser.body) {
            return of(detailUser.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DetailUser());
  }
}

export const detailUserRoute: Routes = [
  {
    path: '',
    component: DetailUserComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'bikeBackendApp.detailUser.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DetailUserDetailComponent,
    resolve: {
      detailUser: DetailUserResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'bikeBackendApp.detailUser.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DetailUserUpdateComponent,
    resolve: {
      detailUser: DetailUserResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'bikeBackendApp.detailUser.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DetailUserUpdateComponent,
    resolve: {
      detailUser: DetailUserResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'bikeBackendApp.detailUser.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
