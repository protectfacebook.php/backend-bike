import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBike, Bike } from 'app/shared/model/bike.model';
import { BikeService } from './bike.service';

@Component({
  selector: 'jhi-bike-update',
  templateUrl: './bike-update.component.html',
})
export class BikeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    model: [null, [Validators.required]],
    price: [],
    serial: [],
    status: [],
    typeShockAbsorber: [],
  });

  constructor(protected bikeService: BikeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bike }) => {
      this.updateForm(bike);
    });
  }

  updateForm(bike: IBike): void {
    this.editForm.patchValue({
      id: bike.id,
      model: bike.model,
      price: bike.price,
      serial: bike.serial,
      status: bike.status,
      typeShockAbsorber: bike.typeShockAbsorber,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bike = this.createFromForm();
    if (bike.id !== undefined) {
      this.subscribeToSaveResponse(this.bikeService.update(bike));
    } else {
      this.subscribeToSaveResponse(this.bikeService.create(bike));
    }
  }

  private createFromForm(): IBike {
    return {
      ...new Bike(),
      id: this.editForm.get(['id'])!.value,
      model: this.editForm.get(['model'])!.value,
      price: this.editForm.get(['price'])!.value,
      serial: this.editForm.get(['serial'])!.value,
      status: this.editForm.get(['status'])!.value,
      typeShockAbsorber: this.editForm.get(['typeShockAbsorber'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBike>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
