import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'detail-user',
        loadChildren: () => import('./detail-user/detail-user.module').then(m => m.BikeBackendDetailUserModule),
      },
      {
        path: 'sale',
        loadChildren: () => import('./sale/sale.module').then(m => m.BikeBackendSaleModule),
      },
      {
        path: 'bike',
        loadChildren: () => import('./bike/bike.module').then(m => m.BikeBackendBikeModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class BikeBackendEntityModule {}
