import { Moment } from 'moment';
import { DocumentType } from 'app/shared/model/enumerations/document-type.model';
import { Gender } from 'app/shared/model/enumerations/gender.model';

export interface IDetailUser {
  id?: number;
  documentType?: DocumentType;
  documentNumber?: string;
  expeditionDate?: Moment;
  cellphone?: string;
  gender?: Gender;
  imageContentType?: string;
  image?: any;
  birthdate?: Moment;
  userFirstName?: string;
  userId?: number;
}

export class DetailUser implements IDetailUser {
  constructor(
    public id?: number,
    public documentType?: DocumentType,
    public documentNumber?: string,
    public expeditionDate?: Moment,
    public cellphone?: string,
    public gender?: Gender,
    public imageContentType?: string,
    public image?: any,
    public birthdate?: Moment,
    public userFirstName?: string,
    public userId?: number
  ) {}
}
