import { Moment } from 'moment';

export interface ISale {
  id?: number;
  dateSale?: Moment;
  bikeModel?: string;
  bikeId?: number;
  clientFirstName?: string;
  clientId?: number;
}

export class Sale implements ISale {
  constructor(
    public id?: number,
    public dateSale?: Moment,
    public bikeModel?: string,
    public bikeId?: number,
    public clientFirstName?: string,
    public clientId?: number
  ) {}
}
