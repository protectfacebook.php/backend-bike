import { TypeShockAbsorber } from 'app/shared/model/enumerations/type-shock-absorber.model';

export interface IBike {
  id?: number;
  model?: string;
  price?: number;
  serial?: string;
  status?: boolean;
  typeShockAbsorber?: TypeShockAbsorber;
}

export class Bike implements IBike {
  constructor(
    public id?: number,
    public model?: string,
    public price?: number,
    public serial?: string,
    public status?: boolean,
    public typeShockAbsorber?: TypeShockAbsorber
  ) {
    this.status = this.status || false;
  }
}
