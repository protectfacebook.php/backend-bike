export const enum Gender {
  MASCULINO = 'MASCULINO',

  FEMENINO = 'FEMENINO',

  OTHER = 'OTHER',
}
