export const enum TypeShockAbsorber {
  RIGIDA = 'RIGIDA',

  HARDTAIL = 'HARDTAIL',

  FULL_SUSPENSION = 'FULL_SUSPENSION',
}
