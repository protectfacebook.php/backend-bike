package com.bikebackend.app.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DetailUserMapperTest {

    private DetailUserMapper detailUserMapper;

    @BeforeEach
    public void setUp() {
        detailUserMapper = new DetailUserMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(detailUserMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(detailUserMapper.fromId(null)).isNull();
    }
}
