package com.bikebackend.app.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BikeMapperTest {

    private BikeMapper bikeMapper;

    @BeforeEach
    public void setUp() {
        bikeMapper = new BikeMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(bikeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(bikeMapper.fromId(null)).isNull();
    }
}
