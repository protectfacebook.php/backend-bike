package com.bikebackend.app.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.bikebackend.app.web.rest.TestUtil;

public class DetailUserDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DetailUserDTO.class);
        DetailUserDTO detailUserDTO1 = new DetailUserDTO();
        detailUserDTO1.setId(1L);
        DetailUserDTO detailUserDTO2 = new DetailUserDTO();
        assertThat(detailUserDTO1).isNotEqualTo(detailUserDTO2);
        detailUserDTO2.setId(detailUserDTO1.getId());
        assertThat(detailUserDTO1).isEqualTo(detailUserDTO2);
        detailUserDTO2.setId(2L);
        assertThat(detailUserDTO1).isNotEqualTo(detailUserDTO2);
        detailUserDTO1.setId(null);
        assertThat(detailUserDTO1).isNotEqualTo(detailUserDTO2);
    }
}
