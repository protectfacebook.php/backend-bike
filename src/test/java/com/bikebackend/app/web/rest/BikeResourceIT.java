package com.bikebackend.app.web.rest;

import com.bikebackend.app.BikeBackendApp;
import com.bikebackend.app.domain.Bike;
import com.bikebackend.app.repository.BikeRepository;
import com.bikebackend.app.service.BikeService;
import com.bikebackend.app.service.dto.BikeDTO;
import com.bikebackend.app.service.mapper.BikeMapper;
import com.bikebackend.app.service.dto.BikeCriteria;
import com.bikebackend.app.service.BikeQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bikebackend.app.domain.enumeration.TypeShockAbsorber;
/**
 * Integration tests for the {@link BikeResource} REST controller.
 */
@SpringBootTest(classes = BikeBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class BikeResourceIT {

    private static final String DEFAULT_MODEL = "AAAAAAAAAA";
    private static final String UPDATED_MODEL = "BBBBBBBBBB";

    private static final Double DEFAULT_PRICE = 1D;
    private static final Double UPDATED_PRICE = 2D;
    private static final Double SMALLER_PRICE = 1D - 1D;

    private static final String DEFAULT_SERIAL = "AAAAAAAAAA";
    private static final String UPDATED_SERIAL = "BBBBBBBBBB";

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final TypeShockAbsorber DEFAULT_TYPE_SHOCK_ABSORBER = TypeShockAbsorber.RIGIDA;
    private static final TypeShockAbsorber UPDATED_TYPE_SHOCK_ABSORBER = TypeShockAbsorber.HARDTAIL;

    @Autowired
    private BikeRepository bikeRepository;

    @Autowired
    private BikeMapper bikeMapper;

    @Autowired
    private BikeService bikeService;

    @Autowired
    private BikeQueryService bikeQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBikeMockMvc;

    private Bike bike;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bike createEntity(EntityManager em) {
        Bike bike = new Bike()
            .model(DEFAULT_MODEL)
            .price(DEFAULT_PRICE)
            .serial(DEFAULT_SERIAL)
            .status(DEFAULT_STATUS)
            .typeShockAbsorber(DEFAULT_TYPE_SHOCK_ABSORBER);
        return bike;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bike createUpdatedEntity(EntityManager em) {
        Bike bike = new Bike()
            .model(UPDATED_MODEL)
            .price(UPDATED_PRICE)
            .serial(UPDATED_SERIAL)
            .status(UPDATED_STATUS)
            .typeShockAbsorber(UPDATED_TYPE_SHOCK_ABSORBER);
        return bike;
    }

    @BeforeEach
    public void initTest() {
        bike = createEntity(em);
    }

    @Test
    @Transactional
    public void createBike() throws Exception {
        int databaseSizeBeforeCreate = bikeRepository.findAll().size();
        // Create the Bike
        BikeDTO bikeDTO = bikeMapper.toDto(bike);
        restBikeMockMvc.perform(post("/api/bikes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bikeDTO)))
            .andExpect(status().isCreated());

        // Validate the Bike in the database
        List<Bike> bikeList = bikeRepository.findAll();
        assertThat(bikeList).hasSize(databaseSizeBeforeCreate + 1);
        Bike testBike = bikeList.get(bikeList.size() - 1);
        assertThat(testBike.getModel()).isEqualTo(DEFAULT_MODEL);
        assertThat(testBike.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testBike.getSerial()).isEqualTo(DEFAULT_SERIAL);
        assertThat(testBike.isStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testBike.getTypeShockAbsorber()).isEqualTo(DEFAULT_TYPE_SHOCK_ABSORBER);
    }

    @Test
    @Transactional
    public void createBikeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bikeRepository.findAll().size();

        // Create the Bike with an existing ID
        bike.setId(1L);
        BikeDTO bikeDTO = bikeMapper.toDto(bike);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBikeMockMvc.perform(post("/api/bikes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bikeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Bike in the database
        List<Bike> bikeList = bikeRepository.findAll();
        assertThat(bikeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkModelIsRequired() throws Exception {
        int databaseSizeBeforeTest = bikeRepository.findAll().size();
        // set the field null
        bike.setModel(null);

        // Create the Bike, which fails.
        BikeDTO bikeDTO = bikeMapper.toDto(bike);


        restBikeMockMvc.perform(post("/api/bikes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bikeDTO)))
            .andExpect(status().isBadRequest());

        List<Bike> bikeList = bikeRepository.findAll();
        assertThat(bikeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBikes() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList
        restBikeMockMvc.perform(get("/api/bikes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bike.getId().intValue())))
            .andExpect(jsonPath("$.[*].model").value(hasItem(DEFAULT_MODEL)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].serial").value(hasItem(DEFAULT_SERIAL)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())))
            .andExpect(jsonPath("$.[*].typeShockAbsorber").value(hasItem(DEFAULT_TYPE_SHOCK_ABSORBER.toString())));
    }
    
    @Test
    @Transactional
    public void getBike() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get the bike
        restBikeMockMvc.perform(get("/api/bikes/{id}", bike.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(bike.getId().intValue()))
            .andExpect(jsonPath("$.model").value(DEFAULT_MODEL))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()))
            .andExpect(jsonPath("$.serial").value(DEFAULT_SERIAL))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()))
            .andExpect(jsonPath("$.typeShockAbsorber").value(DEFAULT_TYPE_SHOCK_ABSORBER.toString()));
    }


    @Test
    @Transactional
    public void getBikesByIdFiltering() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        Long id = bike.getId();

        defaultBikeShouldBeFound("id.equals=" + id);
        defaultBikeShouldNotBeFound("id.notEquals=" + id);

        defaultBikeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultBikeShouldNotBeFound("id.greaterThan=" + id);

        defaultBikeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultBikeShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllBikesByModelIsEqualToSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where model equals to DEFAULT_MODEL
        defaultBikeShouldBeFound("model.equals=" + DEFAULT_MODEL);

        // Get all the bikeList where model equals to UPDATED_MODEL
        defaultBikeShouldNotBeFound("model.equals=" + UPDATED_MODEL);
    }

    @Test
    @Transactional
    public void getAllBikesByModelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where model not equals to DEFAULT_MODEL
        defaultBikeShouldNotBeFound("model.notEquals=" + DEFAULT_MODEL);

        // Get all the bikeList where model not equals to UPDATED_MODEL
        defaultBikeShouldBeFound("model.notEquals=" + UPDATED_MODEL);
    }

    @Test
    @Transactional
    public void getAllBikesByModelIsInShouldWork() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where model in DEFAULT_MODEL or UPDATED_MODEL
        defaultBikeShouldBeFound("model.in=" + DEFAULT_MODEL + "," + UPDATED_MODEL);

        // Get all the bikeList where model equals to UPDATED_MODEL
        defaultBikeShouldNotBeFound("model.in=" + UPDATED_MODEL);
    }

    @Test
    @Transactional
    public void getAllBikesByModelIsNullOrNotNull() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where model is not null
        defaultBikeShouldBeFound("model.specified=true");

        // Get all the bikeList where model is null
        defaultBikeShouldNotBeFound("model.specified=false");
    }
                @Test
    @Transactional
    public void getAllBikesByModelContainsSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where model contains DEFAULT_MODEL
        defaultBikeShouldBeFound("model.contains=" + DEFAULT_MODEL);

        // Get all the bikeList where model contains UPDATED_MODEL
        defaultBikeShouldNotBeFound("model.contains=" + UPDATED_MODEL);
    }

    @Test
    @Transactional
    public void getAllBikesByModelNotContainsSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where model does not contain DEFAULT_MODEL
        defaultBikeShouldNotBeFound("model.doesNotContain=" + DEFAULT_MODEL);

        // Get all the bikeList where model does not contain UPDATED_MODEL
        defaultBikeShouldBeFound("model.doesNotContain=" + UPDATED_MODEL);
    }


    @Test
    @Transactional
    public void getAllBikesByPriceIsEqualToSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where price equals to DEFAULT_PRICE
        defaultBikeShouldBeFound("price.equals=" + DEFAULT_PRICE);

        // Get all the bikeList where price equals to UPDATED_PRICE
        defaultBikeShouldNotBeFound("price.equals=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllBikesByPriceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where price not equals to DEFAULT_PRICE
        defaultBikeShouldNotBeFound("price.notEquals=" + DEFAULT_PRICE);

        // Get all the bikeList where price not equals to UPDATED_PRICE
        defaultBikeShouldBeFound("price.notEquals=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllBikesByPriceIsInShouldWork() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where price in DEFAULT_PRICE or UPDATED_PRICE
        defaultBikeShouldBeFound("price.in=" + DEFAULT_PRICE + "," + UPDATED_PRICE);

        // Get all the bikeList where price equals to UPDATED_PRICE
        defaultBikeShouldNotBeFound("price.in=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllBikesByPriceIsNullOrNotNull() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where price is not null
        defaultBikeShouldBeFound("price.specified=true");

        // Get all the bikeList where price is null
        defaultBikeShouldNotBeFound("price.specified=false");
    }

    @Test
    @Transactional
    public void getAllBikesByPriceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where price is greater than or equal to DEFAULT_PRICE
        defaultBikeShouldBeFound("price.greaterThanOrEqual=" + DEFAULT_PRICE);

        // Get all the bikeList where price is greater than or equal to UPDATED_PRICE
        defaultBikeShouldNotBeFound("price.greaterThanOrEqual=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllBikesByPriceIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where price is less than or equal to DEFAULT_PRICE
        defaultBikeShouldBeFound("price.lessThanOrEqual=" + DEFAULT_PRICE);

        // Get all the bikeList where price is less than or equal to SMALLER_PRICE
        defaultBikeShouldNotBeFound("price.lessThanOrEqual=" + SMALLER_PRICE);
    }

    @Test
    @Transactional
    public void getAllBikesByPriceIsLessThanSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where price is less than DEFAULT_PRICE
        defaultBikeShouldNotBeFound("price.lessThan=" + DEFAULT_PRICE);

        // Get all the bikeList where price is less than UPDATED_PRICE
        defaultBikeShouldBeFound("price.lessThan=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllBikesByPriceIsGreaterThanSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where price is greater than DEFAULT_PRICE
        defaultBikeShouldNotBeFound("price.greaterThan=" + DEFAULT_PRICE);

        // Get all the bikeList where price is greater than SMALLER_PRICE
        defaultBikeShouldBeFound("price.greaterThan=" + SMALLER_PRICE);
    }


    @Test
    @Transactional
    public void getAllBikesBySerialIsEqualToSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where serial equals to DEFAULT_SERIAL
        defaultBikeShouldBeFound("serial.equals=" + DEFAULT_SERIAL);

        // Get all the bikeList where serial equals to UPDATED_SERIAL
        defaultBikeShouldNotBeFound("serial.equals=" + UPDATED_SERIAL);
    }

    @Test
    @Transactional
    public void getAllBikesBySerialIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where serial not equals to DEFAULT_SERIAL
        defaultBikeShouldNotBeFound("serial.notEquals=" + DEFAULT_SERIAL);

        // Get all the bikeList where serial not equals to UPDATED_SERIAL
        defaultBikeShouldBeFound("serial.notEquals=" + UPDATED_SERIAL);
    }

    @Test
    @Transactional
    public void getAllBikesBySerialIsInShouldWork() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where serial in DEFAULT_SERIAL or UPDATED_SERIAL
        defaultBikeShouldBeFound("serial.in=" + DEFAULT_SERIAL + "," + UPDATED_SERIAL);

        // Get all the bikeList where serial equals to UPDATED_SERIAL
        defaultBikeShouldNotBeFound("serial.in=" + UPDATED_SERIAL);
    }

    @Test
    @Transactional
    public void getAllBikesBySerialIsNullOrNotNull() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where serial is not null
        defaultBikeShouldBeFound("serial.specified=true");

        // Get all the bikeList where serial is null
        defaultBikeShouldNotBeFound("serial.specified=false");
    }
                @Test
    @Transactional
    public void getAllBikesBySerialContainsSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where serial contains DEFAULT_SERIAL
        defaultBikeShouldBeFound("serial.contains=" + DEFAULT_SERIAL);

        // Get all the bikeList where serial contains UPDATED_SERIAL
        defaultBikeShouldNotBeFound("serial.contains=" + UPDATED_SERIAL);
    }

    @Test
    @Transactional
    public void getAllBikesBySerialNotContainsSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where serial does not contain DEFAULT_SERIAL
        defaultBikeShouldNotBeFound("serial.doesNotContain=" + DEFAULT_SERIAL);

        // Get all the bikeList where serial does not contain UPDATED_SERIAL
        defaultBikeShouldBeFound("serial.doesNotContain=" + UPDATED_SERIAL);
    }


    @Test
    @Transactional
    public void getAllBikesByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where status equals to DEFAULT_STATUS
        defaultBikeShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the bikeList where status equals to UPDATED_STATUS
        defaultBikeShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllBikesByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where status not equals to DEFAULT_STATUS
        defaultBikeShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the bikeList where status not equals to UPDATED_STATUS
        defaultBikeShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllBikesByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultBikeShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the bikeList where status equals to UPDATED_STATUS
        defaultBikeShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllBikesByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where status is not null
        defaultBikeShouldBeFound("status.specified=true");

        // Get all the bikeList where status is null
        defaultBikeShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllBikesByTypeShockAbsorberIsEqualToSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where typeShockAbsorber equals to DEFAULT_TYPE_SHOCK_ABSORBER
        defaultBikeShouldBeFound("typeShockAbsorber.equals=" + DEFAULT_TYPE_SHOCK_ABSORBER);

        // Get all the bikeList where typeShockAbsorber equals to UPDATED_TYPE_SHOCK_ABSORBER
        defaultBikeShouldNotBeFound("typeShockAbsorber.equals=" + UPDATED_TYPE_SHOCK_ABSORBER);
    }

    @Test
    @Transactional
    public void getAllBikesByTypeShockAbsorberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where typeShockAbsorber not equals to DEFAULT_TYPE_SHOCK_ABSORBER
        defaultBikeShouldNotBeFound("typeShockAbsorber.notEquals=" + DEFAULT_TYPE_SHOCK_ABSORBER);

        // Get all the bikeList where typeShockAbsorber not equals to UPDATED_TYPE_SHOCK_ABSORBER
        defaultBikeShouldBeFound("typeShockAbsorber.notEquals=" + UPDATED_TYPE_SHOCK_ABSORBER);
    }

    @Test
    @Transactional
    public void getAllBikesByTypeShockAbsorberIsInShouldWork() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where typeShockAbsorber in DEFAULT_TYPE_SHOCK_ABSORBER or UPDATED_TYPE_SHOCK_ABSORBER
        defaultBikeShouldBeFound("typeShockAbsorber.in=" + DEFAULT_TYPE_SHOCK_ABSORBER + "," + UPDATED_TYPE_SHOCK_ABSORBER);

        // Get all the bikeList where typeShockAbsorber equals to UPDATED_TYPE_SHOCK_ABSORBER
        defaultBikeShouldNotBeFound("typeShockAbsorber.in=" + UPDATED_TYPE_SHOCK_ABSORBER);
    }

    @Test
    @Transactional
    public void getAllBikesByTypeShockAbsorberIsNullOrNotNull() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        // Get all the bikeList where typeShockAbsorber is not null
        defaultBikeShouldBeFound("typeShockAbsorber.specified=true");

        // Get all the bikeList where typeShockAbsorber is null
        defaultBikeShouldNotBeFound("typeShockAbsorber.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBikeShouldBeFound(String filter) throws Exception {
        restBikeMockMvc.perform(get("/api/bikes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bike.getId().intValue())))
            .andExpect(jsonPath("$.[*].model").value(hasItem(DEFAULT_MODEL)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].serial").value(hasItem(DEFAULT_SERIAL)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())))
            .andExpect(jsonPath("$.[*].typeShockAbsorber").value(hasItem(DEFAULT_TYPE_SHOCK_ABSORBER.toString())));

        // Check, that the count call also returns 1
        restBikeMockMvc.perform(get("/api/bikes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBikeShouldNotBeFound(String filter) throws Exception {
        restBikeMockMvc.perform(get("/api/bikes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBikeMockMvc.perform(get("/api/bikes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingBike() throws Exception {
        // Get the bike
        restBikeMockMvc.perform(get("/api/bikes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBike() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        int databaseSizeBeforeUpdate = bikeRepository.findAll().size();

        // Update the bike
        Bike updatedBike = bikeRepository.findById(bike.getId()).get();
        // Disconnect from session so that the updates on updatedBike are not directly saved in db
        em.detach(updatedBike);
        updatedBike
            .model(UPDATED_MODEL)
            .price(UPDATED_PRICE)
            .serial(UPDATED_SERIAL)
            .status(UPDATED_STATUS)
            .typeShockAbsorber(UPDATED_TYPE_SHOCK_ABSORBER);
        BikeDTO bikeDTO = bikeMapper.toDto(updatedBike);

        restBikeMockMvc.perform(put("/api/bikes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bikeDTO)))
            .andExpect(status().isOk());

        // Validate the Bike in the database
        List<Bike> bikeList = bikeRepository.findAll();
        assertThat(bikeList).hasSize(databaseSizeBeforeUpdate);
        Bike testBike = bikeList.get(bikeList.size() - 1);
        assertThat(testBike.getModel()).isEqualTo(UPDATED_MODEL);
        assertThat(testBike.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testBike.getSerial()).isEqualTo(UPDATED_SERIAL);
        assertThat(testBike.isStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testBike.getTypeShockAbsorber()).isEqualTo(UPDATED_TYPE_SHOCK_ABSORBER);
    }

    @Test
    @Transactional
    public void updateNonExistingBike() throws Exception {
        int databaseSizeBeforeUpdate = bikeRepository.findAll().size();

        // Create the Bike
        BikeDTO bikeDTO = bikeMapper.toDto(bike);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBikeMockMvc.perform(put("/api/bikes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bikeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Bike in the database
        List<Bike> bikeList = bikeRepository.findAll();
        assertThat(bikeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBike() throws Exception {
        // Initialize the database
        bikeRepository.saveAndFlush(bike);

        int databaseSizeBeforeDelete = bikeRepository.findAll().size();

        // Delete the bike
        restBikeMockMvc.perform(delete("/api/bikes/{id}", bike.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Bike> bikeList = bikeRepository.findAll();
        assertThat(bikeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
