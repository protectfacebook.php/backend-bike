package com.bikebackend.app.web.rest;

import com.bikebackend.app.BikeBackendApp;
import com.bikebackend.app.domain.Sale;
import com.bikebackend.app.domain.Bike;
import com.bikebackend.app.domain.User;
import com.bikebackend.app.repository.SaleRepository;
import com.bikebackend.app.service.SaleService;
import com.bikebackend.app.service.dto.SaleDTO;
import com.bikebackend.app.service.mapper.SaleMapper;
import com.bikebackend.app.service.dto.SaleCriteria;
import com.bikebackend.app.service.SaleQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SaleResource} REST controller.
 */
@SpringBootTest(classes = BikeBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class SaleResourceIT {

    private static final Instant DEFAULT_DATE_SALE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_SALE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private SaleRepository saleRepository;

    @Autowired
    private SaleMapper saleMapper;

    @Autowired
    private SaleService saleService;

    @Autowired
    private SaleQueryService saleQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSaleMockMvc;

    private Sale sale;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sale createEntity(EntityManager em) {
        Sale sale = new Sale()
            .dateSale(DEFAULT_DATE_SALE);
        return sale;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sale createUpdatedEntity(EntityManager em) {
        Sale sale = new Sale()
            .dateSale(UPDATED_DATE_SALE);
        return sale;
    }

    @BeforeEach
    public void initTest() {
        sale = createEntity(em);
    }

    @Test
    @Transactional
    public void createSale() throws Exception {
        int databaseSizeBeforeCreate = saleRepository.findAll().size();
        // Create the Sale
        SaleDTO saleDTO = saleMapper.toDto(sale);
        restSaleMockMvc.perform(post("/api/sales")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(saleDTO)))
            .andExpect(status().isCreated());

        // Validate the Sale in the database
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeCreate + 1);
        Sale testSale = saleList.get(saleList.size() - 1);
        assertThat(testSale.getDateSale()).isEqualTo(DEFAULT_DATE_SALE);
    }

    @Test
    @Transactional
    public void createSaleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = saleRepository.findAll().size();

        // Create the Sale with an existing ID
        sale.setId(1L);
        SaleDTO saleDTO = saleMapper.toDto(sale);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSaleMockMvc.perform(post("/api/sales")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(saleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Sale in the database
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSales() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList
        restSaleMockMvc.perform(get("/api/sales?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sale.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateSale").value(hasItem(DEFAULT_DATE_SALE.toString())));
    }
    
    @Test
    @Transactional
    public void getSale() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get the sale
        restSaleMockMvc.perform(get("/api/sales/{id}", sale.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(sale.getId().intValue()))
            .andExpect(jsonPath("$.dateSale").value(DEFAULT_DATE_SALE.toString()));
    }


    @Test
    @Transactional
    public void getSalesByIdFiltering() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        Long id = sale.getId();

        defaultSaleShouldBeFound("id.equals=" + id);
        defaultSaleShouldNotBeFound("id.notEquals=" + id);

        defaultSaleShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultSaleShouldNotBeFound("id.greaterThan=" + id);

        defaultSaleShouldBeFound("id.lessThanOrEqual=" + id);
        defaultSaleShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllSalesByDateSaleIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where dateSale equals to DEFAULT_DATE_SALE
        defaultSaleShouldBeFound("dateSale.equals=" + DEFAULT_DATE_SALE);

        // Get all the saleList where dateSale equals to UPDATED_DATE_SALE
        defaultSaleShouldNotBeFound("dateSale.equals=" + UPDATED_DATE_SALE);
    }

    @Test
    @Transactional
    public void getAllSalesByDateSaleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where dateSale not equals to DEFAULT_DATE_SALE
        defaultSaleShouldNotBeFound("dateSale.notEquals=" + DEFAULT_DATE_SALE);

        // Get all the saleList where dateSale not equals to UPDATED_DATE_SALE
        defaultSaleShouldBeFound("dateSale.notEquals=" + UPDATED_DATE_SALE);
    }

    @Test
    @Transactional
    public void getAllSalesByDateSaleIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where dateSale in DEFAULT_DATE_SALE or UPDATED_DATE_SALE
        defaultSaleShouldBeFound("dateSale.in=" + DEFAULT_DATE_SALE + "," + UPDATED_DATE_SALE);

        // Get all the saleList where dateSale equals to UPDATED_DATE_SALE
        defaultSaleShouldNotBeFound("dateSale.in=" + UPDATED_DATE_SALE);
    }

    @Test
    @Transactional
    public void getAllSalesByDateSaleIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where dateSale is not null
        defaultSaleShouldBeFound("dateSale.specified=true");

        // Get all the saleList where dateSale is null
        defaultSaleShouldNotBeFound("dateSale.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByBikeIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);
        Bike bike = BikeResourceIT.createEntity(em);
        em.persist(bike);
        em.flush();
        sale.setBike(bike);
        saleRepository.saveAndFlush(sale);
        Long bikeId = bike.getId();

        // Get all the saleList where bike equals to bikeId
        defaultSaleShouldBeFound("bikeId.equals=" + bikeId);

        // Get all the saleList where bike equals to bikeId + 1
        defaultSaleShouldNotBeFound("bikeId.equals=" + (bikeId + 1));
    }


    @Test
    @Transactional
    public void getAllSalesByClientIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);
        User client = UserResourceIT.createEntity(em);
        em.persist(client);
        em.flush();
        sale.setClient(client);
        saleRepository.saveAndFlush(sale);
        Long clientId = client.getId();

        // Get all the saleList where client equals to clientId
        defaultSaleShouldBeFound("clientId.equals=" + clientId);

        // Get all the saleList where client equals to clientId + 1
        defaultSaleShouldNotBeFound("clientId.equals=" + (clientId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSaleShouldBeFound(String filter) throws Exception {
        restSaleMockMvc.perform(get("/api/sales?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sale.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateSale").value(hasItem(DEFAULT_DATE_SALE.toString())));

        // Check, that the count call also returns 1
        restSaleMockMvc.perform(get("/api/sales/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSaleShouldNotBeFound(String filter) throws Exception {
        restSaleMockMvc.perform(get("/api/sales?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSaleMockMvc.perform(get("/api/sales/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingSale() throws Exception {
        // Get the sale
        restSaleMockMvc.perform(get("/api/sales/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSale() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        int databaseSizeBeforeUpdate = saleRepository.findAll().size();

        // Update the sale
        Sale updatedSale = saleRepository.findById(sale.getId()).get();
        // Disconnect from session so that the updates on updatedSale are not directly saved in db
        em.detach(updatedSale);
        updatedSale
            .dateSale(UPDATED_DATE_SALE);
        SaleDTO saleDTO = saleMapper.toDto(updatedSale);

        restSaleMockMvc.perform(put("/api/sales")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(saleDTO)))
            .andExpect(status().isOk());

        // Validate the Sale in the database
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeUpdate);
        Sale testSale = saleList.get(saleList.size() - 1);
        assertThat(testSale.getDateSale()).isEqualTo(UPDATED_DATE_SALE);
    }

    @Test
    @Transactional
    public void updateNonExistingSale() throws Exception {
        int databaseSizeBeforeUpdate = saleRepository.findAll().size();

        // Create the Sale
        SaleDTO saleDTO = saleMapper.toDto(sale);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSaleMockMvc.perform(put("/api/sales")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(saleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Sale in the database
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSale() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        int databaseSizeBeforeDelete = saleRepository.findAll().size();

        // Delete the sale
        restSaleMockMvc.perform(delete("/api/sales/{id}", sale.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
