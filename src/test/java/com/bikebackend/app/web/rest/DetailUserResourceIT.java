package com.bikebackend.app.web.rest;

import com.bikebackend.app.BikeBackendApp;
import com.bikebackend.app.domain.DetailUser;
import com.bikebackend.app.domain.User;
import com.bikebackend.app.repository.DetailUserRepository;
import com.bikebackend.app.service.DetailUserService;
import com.bikebackend.app.service.dto.DetailUserDTO;
import com.bikebackend.app.service.mapper.DetailUserMapper;
import com.bikebackend.app.service.dto.DetailUserCriteria;
import com.bikebackend.app.service.DetailUserQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bikebackend.app.domain.enumeration.DocumentType;
import com.bikebackend.app.domain.enumeration.Gender;
/**
 * Integration tests for the {@link DetailUserResource} REST controller.
 */
@SpringBootTest(classes = BikeBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class DetailUserResourceIT {

    private static final DocumentType DEFAULT_DOCUMENT_TYPE = DocumentType.CC;
    private static final DocumentType UPDATED_DOCUMENT_TYPE = DocumentType.CE;

    private static final String DEFAULT_DOCUMENT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_DOCUMENT_NUMBER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_EXPEDITION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EXPEDITION_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_EXPEDITION_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_CELLPHONE = "AAAAAAAAAA";
    private static final String UPDATED_CELLPHONE = "BBBBBBBBBB";

    private static final Gender DEFAULT_GENDER = Gender.MASCULINO;
    private static final Gender UPDATED_GENDER = Gender.FEMENINO;

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    private static final LocalDate DEFAULT_BIRTHDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTHDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_BIRTHDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private DetailUserRepository detailUserRepository;

    @Autowired
    private DetailUserMapper detailUserMapper;

    @Autowired
    private DetailUserService detailUserService;

    @Autowired
    private DetailUserQueryService detailUserQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDetailUserMockMvc;

    private DetailUser detailUser;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DetailUser createEntity(EntityManager em) {
        DetailUser detailUser = new DetailUser()
            .documentType(DEFAULT_DOCUMENT_TYPE)
            .documentNumber(DEFAULT_DOCUMENT_NUMBER)
            .expeditionDate(DEFAULT_EXPEDITION_DATE)
            .cellphone(DEFAULT_CELLPHONE)
            .gender(DEFAULT_GENDER)
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE)
            .birthdate(DEFAULT_BIRTHDATE);
        return detailUser;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DetailUser createUpdatedEntity(EntityManager em) {
        DetailUser detailUser = new DetailUser()
            .documentType(UPDATED_DOCUMENT_TYPE)
            .documentNumber(UPDATED_DOCUMENT_NUMBER)
            .expeditionDate(UPDATED_EXPEDITION_DATE)
            .cellphone(UPDATED_CELLPHONE)
            .gender(UPDATED_GENDER)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .birthdate(UPDATED_BIRTHDATE);
        return detailUser;
    }

    @BeforeEach
    public void initTest() {
        detailUser = createEntity(em);
    }

    @Test
    @Transactional
    public void createDetailUser() throws Exception {
        int databaseSizeBeforeCreate = detailUserRepository.findAll().size();
        // Create the DetailUser
        DetailUserDTO detailUserDTO = detailUserMapper.toDto(detailUser);
        restDetailUserMockMvc.perform(post("/api/detail-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailUserDTO)))
            .andExpect(status().isCreated());

        // Validate the DetailUser in the database
        List<DetailUser> detailUserList = detailUserRepository.findAll();
        assertThat(detailUserList).hasSize(databaseSizeBeforeCreate + 1);
        DetailUser testDetailUser = detailUserList.get(detailUserList.size() - 1);
        assertThat(testDetailUser.getDocumentType()).isEqualTo(DEFAULT_DOCUMENT_TYPE);
        assertThat(testDetailUser.getDocumentNumber()).isEqualTo(DEFAULT_DOCUMENT_NUMBER);
        assertThat(testDetailUser.getExpeditionDate()).isEqualTo(DEFAULT_EXPEDITION_DATE);
        assertThat(testDetailUser.getCellphone()).isEqualTo(DEFAULT_CELLPHONE);
        assertThat(testDetailUser.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testDetailUser.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testDetailUser.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
        assertThat(testDetailUser.getBirthdate()).isEqualTo(DEFAULT_BIRTHDATE);
    }

    @Test
    @Transactional
    public void createDetailUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = detailUserRepository.findAll().size();

        // Create the DetailUser with an existing ID
        detailUser.setId(1L);
        DetailUserDTO detailUserDTO = detailUserMapper.toDto(detailUser);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDetailUserMockMvc.perform(post("/api/detail-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailUserDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DetailUser in the database
        List<DetailUser> detailUserList = detailUserRepository.findAll();
        assertThat(detailUserList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDetailUsers() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList
        restDetailUserMockMvc.perform(get("/api/detail-users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(detailUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].documentType").value(hasItem(DEFAULT_DOCUMENT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].documentNumber").value(hasItem(DEFAULT_DOCUMENT_NUMBER)))
            .andExpect(jsonPath("$.[*].expeditionDate").value(hasItem(DEFAULT_EXPEDITION_DATE.toString())))
            .andExpect(jsonPath("$.[*].cellphone").value(hasItem(DEFAULT_CELLPHONE)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))))
            .andExpect(jsonPath("$.[*].birthdate").value(hasItem(DEFAULT_BIRTHDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getDetailUser() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get the detailUser
        restDetailUserMockMvc.perform(get("/api/detail-users/{id}", detailUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(detailUser.getId().intValue()))
            .andExpect(jsonPath("$.documentType").value(DEFAULT_DOCUMENT_TYPE.toString()))
            .andExpect(jsonPath("$.documentNumber").value(DEFAULT_DOCUMENT_NUMBER))
            .andExpect(jsonPath("$.expeditionDate").value(DEFAULT_EXPEDITION_DATE.toString()))
            .andExpect(jsonPath("$.cellphone").value(DEFAULT_CELLPHONE))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64Utils.encodeToString(DEFAULT_IMAGE)))
            .andExpect(jsonPath("$.birthdate").value(DEFAULT_BIRTHDATE.toString()));
    }


    @Test
    @Transactional
    public void getDetailUsersByIdFiltering() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        Long id = detailUser.getId();

        defaultDetailUserShouldBeFound("id.equals=" + id);
        defaultDetailUserShouldNotBeFound("id.notEquals=" + id);

        defaultDetailUserShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDetailUserShouldNotBeFound("id.greaterThan=" + id);

        defaultDetailUserShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDetailUserShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllDetailUsersByDocumentTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where documentType equals to DEFAULT_DOCUMENT_TYPE
        defaultDetailUserShouldBeFound("documentType.equals=" + DEFAULT_DOCUMENT_TYPE);

        // Get all the detailUserList where documentType equals to UPDATED_DOCUMENT_TYPE
        defaultDetailUserShouldNotBeFound("documentType.equals=" + UPDATED_DOCUMENT_TYPE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByDocumentTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where documentType not equals to DEFAULT_DOCUMENT_TYPE
        defaultDetailUserShouldNotBeFound("documentType.notEquals=" + DEFAULT_DOCUMENT_TYPE);

        // Get all the detailUserList where documentType not equals to UPDATED_DOCUMENT_TYPE
        defaultDetailUserShouldBeFound("documentType.notEquals=" + UPDATED_DOCUMENT_TYPE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByDocumentTypeIsInShouldWork() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where documentType in DEFAULT_DOCUMENT_TYPE or UPDATED_DOCUMENT_TYPE
        defaultDetailUserShouldBeFound("documentType.in=" + DEFAULT_DOCUMENT_TYPE + "," + UPDATED_DOCUMENT_TYPE);

        // Get all the detailUserList where documentType equals to UPDATED_DOCUMENT_TYPE
        defaultDetailUserShouldNotBeFound("documentType.in=" + UPDATED_DOCUMENT_TYPE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByDocumentTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where documentType is not null
        defaultDetailUserShouldBeFound("documentType.specified=true");

        // Get all the detailUserList where documentType is null
        defaultDetailUserShouldNotBeFound("documentType.specified=false");
    }

    @Test
    @Transactional
    public void getAllDetailUsersByDocumentNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where documentNumber equals to DEFAULT_DOCUMENT_NUMBER
        defaultDetailUserShouldBeFound("documentNumber.equals=" + DEFAULT_DOCUMENT_NUMBER);

        // Get all the detailUserList where documentNumber equals to UPDATED_DOCUMENT_NUMBER
        defaultDetailUserShouldNotBeFound("documentNumber.equals=" + UPDATED_DOCUMENT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByDocumentNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where documentNumber not equals to DEFAULT_DOCUMENT_NUMBER
        defaultDetailUserShouldNotBeFound("documentNumber.notEquals=" + DEFAULT_DOCUMENT_NUMBER);

        // Get all the detailUserList where documentNumber not equals to UPDATED_DOCUMENT_NUMBER
        defaultDetailUserShouldBeFound("documentNumber.notEquals=" + UPDATED_DOCUMENT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByDocumentNumberIsInShouldWork() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where documentNumber in DEFAULT_DOCUMENT_NUMBER or UPDATED_DOCUMENT_NUMBER
        defaultDetailUserShouldBeFound("documentNumber.in=" + DEFAULT_DOCUMENT_NUMBER + "," + UPDATED_DOCUMENT_NUMBER);

        // Get all the detailUserList where documentNumber equals to UPDATED_DOCUMENT_NUMBER
        defaultDetailUserShouldNotBeFound("documentNumber.in=" + UPDATED_DOCUMENT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByDocumentNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where documentNumber is not null
        defaultDetailUserShouldBeFound("documentNumber.specified=true");

        // Get all the detailUserList where documentNumber is null
        defaultDetailUserShouldNotBeFound("documentNumber.specified=false");
    }
                @Test
    @Transactional
    public void getAllDetailUsersByDocumentNumberContainsSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where documentNumber contains DEFAULT_DOCUMENT_NUMBER
        defaultDetailUserShouldBeFound("documentNumber.contains=" + DEFAULT_DOCUMENT_NUMBER);

        // Get all the detailUserList where documentNumber contains UPDATED_DOCUMENT_NUMBER
        defaultDetailUserShouldNotBeFound("documentNumber.contains=" + UPDATED_DOCUMENT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByDocumentNumberNotContainsSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where documentNumber does not contain DEFAULT_DOCUMENT_NUMBER
        defaultDetailUserShouldNotBeFound("documentNumber.doesNotContain=" + DEFAULT_DOCUMENT_NUMBER);

        // Get all the detailUserList where documentNumber does not contain UPDATED_DOCUMENT_NUMBER
        defaultDetailUserShouldBeFound("documentNumber.doesNotContain=" + UPDATED_DOCUMENT_NUMBER);
    }


    @Test
    @Transactional
    public void getAllDetailUsersByExpeditionDateIsEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where expeditionDate equals to DEFAULT_EXPEDITION_DATE
        defaultDetailUserShouldBeFound("expeditionDate.equals=" + DEFAULT_EXPEDITION_DATE);

        // Get all the detailUserList where expeditionDate equals to UPDATED_EXPEDITION_DATE
        defaultDetailUserShouldNotBeFound("expeditionDate.equals=" + UPDATED_EXPEDITION_DATE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByExpeditionDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where expeditionDate not equals to DEFAULT_EXPEDITION_DATE
        defaultDetailUserShouldNotBeFound("expeditionDate.notEquals=" + DEFAULT_EXPEDITION_DATE);

        // Get all the detailUserList where expeditionDate not equals to UPDATED_EXPEDITION_DATE
        defaultDetailUserShouldBeFound("expeditionDate.notEquals=" + UPDATED_EXPEDITION_DATE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByExpeditionDateIsInShouldWork() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where expeditionDate in DEFAULT_EXPEDITION_DATE or UPDATED_EXPEDITION_DATE
        defaultDetailUserShouldBeFound("expeditionDate.in=" + DEFAULT_EXPEDITION_DATE + "," + UPDATED_EXPEDITION_DATE);

        // Get all the detailUserList where expeditionDate equals to UPDATED_EXPEDITION_DATE
        defaultDetailUserShouldNotBeFound("expeditionDate.in=" + UPDATED_EXPEDITION_DATE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByExpeditionDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where expeditionDate is not null
        defaultDetailUserShouldBeFound("expeditionDate.specified=true");

        // Get all the detailUserList where expeditionDate is null
        defaultDetailUserShouldNotBeFound("expeditionDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllDetailUsersByExpeditionDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where expeditionDate is greater than or equal to DEFAULT_EXPEDITION_DATE
        defaultDetailUserShouldBeFound("expeditionDate.greaterThanOrEqual=" + DEFAULT_EXPEDITION_DATE);

        // Get all the detailUserList where expeditionDate is greater than or equal to UPDATED_EXPEDITION_DATE
        defaultDetailUserShouldNotBeFound("expeditionDate.greaterThanOrEqual=" + UPDATED_EXPEDITION_DATE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByExpeditionDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where expeditionDate is less than or equal to DEFAULT_EXPEDITION_DATE
        defaultDetailUserShouldBeFound("expeditionDate.lessThanOrEqual=" + DEFAULT_EXPEDITION_DATE);

        // Get all the detailUserList where expeditionDate is less than or equal to SMALLER_EXPEDITION_DATE
        defaultDetailUserShouldNotBeFound("expeditionDate.lessThanOrEqual=" + SMALLER_EXPEDITION_DATE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByExpeditionDateIsLessThanSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where expeditionDate is less than DEFAULT_EXPEDITION_DATE
        defaultDetailUserShouldNotBeFound("expeditionDate.lessThan=" + DEFAULT_EXPEDITION_DATE);

        // Get all the detailUserList where expeditionDate is less than UPDATED_EXPEDITION_DATE
        defaultDetailUserShouldBeFound("expeditionDate.lessThan=" + UPDATED_EXPEDITION_DATE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByExpeditionDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where expeditionDate is greater than DEFAULT_EXPEDITION_DATE
        defaultDetailUserShouldNotBeFound("expeditionDate.greaterThan=" + DEFAULT_EXPEDITION_DATE);

        // Get all the detailUserList where expeditionDate is greater than SMALLER_EXPEDITION_DATE
        defaultDetailUserShouldBeFound("expeditionDate.greaterThan=" + SMALLER_EXPEDITION_DATE);
    }


    @Test
    @Transactional
    public void getAllDetailUsersByCellphoneIsEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where cellphone equals to DEFAULT_CELLPHONE
        defaultDetailUserShouldBeFound("cellphone.equals=" + DEFAULT_CELLPHONE);

        // Get all the detailUserList where cellphone equals to UPDATED_CELLPHONE
        defaultDetailUserShouldNotBeFound("cellphone.equals=" + UPDATED_CELLPHONE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByCellphoneIsNotEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where cellphone not equals to DEFAULT_CELLPHONE
        defaultDetailUserShouldNotBeFound("cellphone.notEquals=" + DEFAULT_CELLPHONE);

        // Get all the detailUserList where cellphone not equals to UPDATED_CELLPHONE
        defaultDetailUserShouldBeFound("cellphone.notEquals=" + UPDATED_CELLPHONE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByCellphoneIsInShouldWork() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where cellphone in DEFAULT_CELLPHONE or UPDATED_CELLPHONE
        defaultDetailUserShouldBeFound("cellphone.in=" + DEFAULT_CELLPHONE + "," + UPDATED_CELLPHONE);

        // Get all the detailUserList where cellphone equals to UPDATED_CELLPHONE
        defaultDetailUserShouldNotBeFound("cellphone.in=" + UPDATED_CELLPHONE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByCellphoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where cellphone is not null
        defaultDetailUserShouldBeFound("cellphone.specified=true");

        // Get all the detailUserList where cellphone is null
        defaultDetailUserShouldNotBeFound("cellphone.specified=false");
    }
                @Test
    @Transactional
    public void getAllDetailUsersByCellphoneContainsSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where cellphone contains DEFAULT_CELLPHONE
        defaultDetailUserShouldBeFound("cellphone.contains=" + DEFAULT_CELLPHONE);

        // Get all the detailUserList where cellphone contains UPDATED_CELLPHONE
        defaultDetailUserShouldNotBeFound("cellphone.contains=" + UPDATED_CELLPHONE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByCellphoneNotContainsSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where cellphone does not contain DEFAULT_CELLPHONE
        defaultDetailUserShouldNotBeFound("cellphone.doesNotContain=" + DEFAULT_CELLPHONE);

        // Get all the detailUserList where cellphone does not contain UPDATED_CELLPHONE
        defaultDetailUserShouldBeFound("cellphone.doesNotContain=" + UPDATED_CELLPHONE);
    }


    @Test
    @Transactional
    public void getAllDetailUsersByGenderIsEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where gender equals to DEFAULT_GENDER
        defaultDetailUserShouldBeFound("gender.equals=" + DEFAULT_GENDER);

        // Get all the detailUserList where gender equals to UPDATED_GENDER
        defaultDetailUserShouldNotBeFound("gender.equals=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByGenderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where gender not equals to DEFAULT_GENDER
        defaultDetailUserShouldNotBeFound("gender.notEquals=" + DEFAULT_GENDER);

        // Get all the detailUserList where gender not equals to UPDATED_GENDER
        defaultDetailUserShouldBeFound("gender.notEquals=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByGenderIsInShouldWork() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where gender in DEFAULT_GENDER or UPDATED_GENDER
        defaultDetailUserShouldBeFound("gender.in=" + DEFAULT_GENDER + "," + UPDATED_GENDER);

        // Get all the detailUserList where gender equals to UPDATED_GENDER
        defaultDetailUserShouldNotBeFound("gender.in=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByGenderIsNullOrNotNull() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where gender is not null
        defaultDetailUserShouldBeFound("gender.specified=true");

        // Get all the detailUserList where gender is null
        defaultDetailUserShouldNotBeFound("gender.specified=false");
    }

    @Test
    @Transactional
    public void getAllDetailUsersByBirthdateIsEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where birthdate equals to DEFAULT_BIRTHDATE
        defaultDetailUserShouldBeFound("birthdate.equals=" + DEFAULT_BIRTHDATE);

        // Get all the detailUserList where birthdate equals to UPDATED_BIRTHDATE
        defaultDetailUserShouldNotBeFound("birthdate.equals=" + UPDATED_BIRTHDATE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByBirthdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where birthdate not equals to DEFAULT_BIRTHDATE
        defaultDetailUserShouldNotBeFound("birthdate.notEquals=" + DEFAULT_BIRTHDATE);

        // Get all the detailUserList where birthdate not equals to UPDATED_BIRTHDATE
        defaultDetailUserShouldBeFound("birthdate.notEquals=" + UPDATED_BIRTHDATE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByBirthdateIsInShouldWork() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where birthdate in DEFAULT_BIRTHDATE or UPDATED_BIRTHDATE
        defaultDetailUserShouldBeFound("birthdate.in=" + DEFAULT_BIRTHDATE + "," + UPDATED_BIRTHDATE);

        // Get all the detailUserList where birthdate equals to UPDATED_BIRTHDATE
        defaultDetailUserShouldNotBeFound("birthdate.in=" + UPDATED_BIRTHDATE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByBirthdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where birthdate is not null
        defaultDetailUserShouldBeFound("birthdate.specified=true");

        // Get all the detailUserList where birthdate is null
        defaultDetailUserShouldNotBeFound("birthdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllDetailUsersByBirthdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where birthdate is greater than or equal to DEFAULT_BIRTHDATE
        defaultDetailUserShouldBeFound("birthdate.greaterThanOrEqual=" + DEFAULT_BIRTHDATE);

        // Get all the detailUserList where birthdate is greater than or equal to UPDATED_BIRTHDATE
        defaultDetailUserShouldNotBeFound("birthdate.greaterThanOrEqual=" + UPDATED_BIRTHDATE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByBirthdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where birthdate is less than or equal to DEFAULT_BIRTHDATE
        defaultDetailUserShouldBeFound("birthdate.lessThanOrEqual=" + DEFAULT_BIRTHDATE);

        // Get all the detailUserList where birthdate is less than or equal to SMALLER_BIRTHDATE
        defaultDetailUserShouldNotBeFound("birthdate.lessThanOrEqual=" + SMALLER_BIRTHDATE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByBirthdateIsLessThanSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where birthdate is less than DEFAULT_BIRTHDATE
        defaultDetailUserShouldNotBeFound("birthdate.lessThan=" + DEFAULT_BIRTHDATE);

        // Get all the detailUserList where birthdate is less than UPDATED_BIRTHDATE
        defaultDetailUserShouldBeFound("birthdate.lessThan=" + UPDATED_BIRTHDATE);
    }

    @Test
    @Transactional
    public void getAllDetailUsersByBirthdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        // Get all the detailUserList where birthdate is greater than DEFAULT_BIRTHDATE
        defaultDetailUserShouldNotBeFound("birthdate.greaterThan=" + DEFAULT_BIRTHDATE);

        // Get all the detailUserList where birthdate is greater than SMALLER_BIRTHDATE
        defaultDetailUserShouldBeFound("birthdate.greaterThan=" + SMALLER_BIRTHDATE);
    }


    @Test
    @Transactional
    public void getAllDetailUsersByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        detailUser.setUser(user);
        detailUserRepository.saveAndFlush(detailUser);
        Long userId = user.getId();

        // Get all the detailUserList where user equals to userId
        defaultDetailUserShouldBeFound("userId.equals=" + userId);

        // Get all the detailUserList where user equals to userId + 1
        defaultDetailUserShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDetailUserShouldBeFound(String filter) throws Exception {
        restDetailUserMockMvc.perform(get("/api/detail-users?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(detailUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].documentType").value(hasItem(DEFAULT_DOCUMENT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].documentNumber").value(hasItem(DEFAULT_DOCUMENT_NUMBER)))
            .andExpect(jsonPath("$.[*].expeditionDate").value(hasItem(DEFAULT_EXPEDITION_DATE.toString())))
            .andExpect(jsonPath("$.[*].cellphone").value(hasItem(DEFAULT_CELLPHONE)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))))
            .andExpect(jsonPath("$.[*].birthdate").value(hasItem(DEFAULT_BIRTHDATE.toString())));

        // Check, that the count call also returns 1
        restDetailUserMockMvc.perform(get("/api/detail-users/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDetailUserShouldNotBeFound(String filter) throws Exception {
        restDetailUserMockMvc.perform(get("/api/detail-users?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDetailUserMockMvc.perform(get("/api/detail-users/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingDetailUser() throws Exception {
        // Get the detailUser
        restDetailUserMockMvc.perform(get("/api/detail-users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDetailUser() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        int databaseSizeBeforeUpdate = detailUserRepository.findAll().size();

        // Update the detailUser
        DetailUser updatedDetailUser = detailUserRepository.findById(detailUser.getId()).get();
        // Disconnect from session so that the updates on updatedDetailUser are not directly saved in db
        em.detach(updatedDetailUser);
        updatedDetailUser
            .documentType(UPDATED_DOCUMENT_TYPE)
            .documentNumber(UPDATED_DOCUMENT_NUMBER)
            .expeditionDate(UPDATED_EXPEDITION_DATE)
            .cellphone(UPDATED_CELLPHONE)
            .gender(UPDATED_GENDER)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .birthdate(UPDATED_BIRTHDATE);
        DetailUserDTO detailUserDTO = detailUserMapper.toDto(updatedDetailUser);

        restDetailUserMockMvc.perform(put("/api/detail-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailUserDTO)))
            .andExpect(status().isOk());

        // Validate the DetailUser in the database
        List<DetailUser> detailUserList = detailUserRepository.findAll();
        assertThat(detailUserList).hasSize(databaseSizeBeforeUpdate);
        DetailUser testDetailUser = detailUserList.get(detailUserList.size() - 1);
        assertThat(testDetailUser.getDocumentType()).isEqualTo(UPDATED_DOCUMENT_TYPE);
        assertThat(testDetailUser.getDocumentNumber()).isEqualTo(UPDATED_DOCUMENT_NUMBER);
        assertThat(testDetailUser.getExpeditionDate()).isEqualTo(UPDATED_EXPEDITION_DATE);
        assertThat(testDetailUser.getCellphone()).isEqualTo(UPDATED_CELLPHONE);
        assertThat(testDetailUser.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testDetailUser.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testDetailUser.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
        assertThat(testDetailUser.getBirthdate()).isEqualTo(UPDATED_BIRTHDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingDetailUser() throws Exception {
        int databaseSizeBeforeUpdate = detailUserRepository.findAll().size();

        // Create the DetailUser
        DetailUserDTO detailUserDTO = detailUserMapper.toDto(detailUser);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDetailUserMockMvc.perform(put("/api/detail-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailUserDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DetailUser in the database
        List<DetailUser> detailUserList = detailUserRepository.findAll();
        assertThat(detailUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDetailUser() throws Exception {
        // Initialize the database
        detailUserRepository.saveAndFlush(detailUser);

        int databaseSizeBeforeDelete = detailUserRepository.findAll().size();

        // Delete the detailUser
        restDetailUserMockMvc.perform(delete("/api/detail-users/{id}", detailUser.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DetailUser> detailUserList = detailUserRepository.findAll();
        assertThat(detailUserList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
