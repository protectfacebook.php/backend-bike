package com.bikebackend.app.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.bikebackend.app.web.rest.TestUtil;

public class DetailUserTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DetailUser.class);
        DetailUser detailUser1 = new DetailUser();
        detailUser1.setId(1L);
        DetailUser detailUser2 = new DetailUser();
        detailUser2.setId(detailUser1.getId());
        assertThat(detailUser1).isEqualTo(detailUser2);
        detailUser2.setId(2L);
        assertThat(detailUser1).isNotEqualTo(detailUser2);
        detailUser1.setId(null);
        assertThat(detailUser1).isNotEqualTo(detailUser2);
    }
}
