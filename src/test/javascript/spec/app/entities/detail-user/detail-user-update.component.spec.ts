import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { BikeBackendTestModule } from '../../../test.module';
import { DetailUserUpdateComponent } from 'app/entities/detail-user/detail-user-update.component';
import { DetailUserService } from 'app/entities/detail-user/detail-user.service';
import { DetailUser } from 'app/shared/model/detail-user.model';

describe('Component Tests', () => {
  describe('DetailUser Management Update Component', () => {
    let comp: DetailUserUpdateComponent;
    let fixture: ComponentFixture<DetailUserUpdateComponent>;
    let service: DetailUserService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BikeBackendTestModule],
        declarations: [DetailUserUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(DetailUserUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DetailUserUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DetailUserService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new DetailUser(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new DetailUser();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
