import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { DetailUserService } from 'app/entities/detail-user/detail-user.service';
import { IDetailUser, DetailUser } from 'app/shared/model/detail-user.model';
import { DocumentType } from 'app/shared/model/enumerations/document-type.model';
import { Gender } from 'app/shared/model/enumerations/gender.model';

describe('Service Tests', () => {
  describe('DetailUser Service', () => {
    let injector: TestBed;
    let service: DetailUserService;
    let httpMock: HttpTestingController;
    let elemDefault: IDetailUser;
    let expectedResult: IDetailUser | IDetailUser[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(DetailUserService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new DetailUser(
        0,
        DocumentType.CC,
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        Gender.MASCULINO,
        'image/png',
        'AAAAAAA',
        currentDate
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            expeditionDate: currentDate.format(DATE_FORMAT),
            birthdate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DetailUser', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            expeditionDate: currentDate.format(DATE_FORMAT),
            birthdate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            expeditionDate: currentDate,
            birthdate: currentDate,
          },
          returnedFromService
        );

        service.create(new DetailUser()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DetailUser', () => {
        const returnedFromService = Object.assign(
          {
            documentType: 'BBBBBB',
            documentNumber: 'BBBBBB',
            expeditionDate: currentDate.format(DATE_FORMAT),
            cellphone: 'BBBBBB',
            gender: 'BBBBBB',
            image: 'BBBBBB',
            birthdate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            expeditionDate: currentDate,
            birthdate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DetailUser', () => {
        const returnedFromService = Object.assign(
          {
            documentType: 'BBBBBB',
            documentNumber: 'BBBBBB',
            expeditionDate: currentDate.format(DATE_FORMAT),
            cellphone: 'BBBBBB',
            gender: 'BBBBBB',
            image: 'BBBBBB',
            birthdate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            expeditionDate: currentDate,
            birthdate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DetailUser', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
