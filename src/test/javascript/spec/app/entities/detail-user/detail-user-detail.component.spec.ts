import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { BikeBackendTestModule } from '../../../test.module';
import { DetailUserDetailComponent } from 'app/entities/detail-user/detail-user-detail.component';
import { DetailUser } from 'app/shared/model/detail-user.model';

describe('Component Tests', () => {
  describe('DetailUser Management Detail Component', () => {
    let comp: DetailUserDetailComponent;
    let fixture: ComponentFixture<DetailUserDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ detailUser: new DetailUser(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BikeBackendTestModule],
        declarations: [DetailUserDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(DetailUserDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DetailUserDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load detailUser on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.detailUser).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
